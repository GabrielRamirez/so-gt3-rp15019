.section .data
msg:
	.string "Numero %d: "
res:
	.string "La suma es: %d\n"
fmt:
	.string "%d"

.section .text
	.global main

main:
	pushl %ebp
	movl %esp, %ebp

	pushl $1
	pushl $msg
	call printf

	leal (%esp), %eax
	movl %eax, 4(%esp)
	movl $fmt, (%esp)
	call scanf

	movl (%esp), %ebx

	pushl $2
	pushl $msg
	call printf

	leal (%esp), %eax
	movl %eax, 4(%esp)
	movl $fmt, (%esp)
	call scanf

	addl (%esp),%ebx

	pushl %ebx
	pushl $res
	call printf

	movl %ebp, %esp
	popl %ebp
	ret
